package org.komissarova.many_files;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class NumberIO {

    /**
     * Записывает числа из спиcка numbers в файл fileName 
     * 
     * @param numbers список чисел
     * @param fileName имя файла, в который будут записаны данные числа
     */
    public static ArrayList<Integer> read(String fileName) {
        ArrayList<Integer> numbers = new ArrayList<>();
        
        try(DataInputStream in = new DataInputStream(new FileInputStream(fileName))) {
            while(in.available() > 0) {
                numbers.add(in.readInt());
            }
        } catch (IOException exception) {
            System.out.println("Что-то пошло не так");
        }
        
        return numbers;
    }

    /**
     * Записывает числа из спиcка numbers в файл fileName в виде СТРОК
     * FileWriter позволяет записывать данные в текстовый файл. С его помощью невозможно записать число как последовательность байт,
     * а только как строку. Отлично подходит для представления результатов работы нашей программы
     * 
     * @param numbers список чисел
     * @param fileName имя файла, в который будут записаны данные числа
     */
    public static void writeAsText(ArrayList<Integer> numbers, String fileName) {
        try(FileWriter writer = new FileWriter(fileName)) {
            for(Integer number : numbers) {
                writer.write(number + "\n");
            }
        } catch (IOException exception) {
            System.out.println("Что-то пошло не так");
        }
    }

    /**
     * Записывает числа из спиcка numbers в файл fileName в вице ЧИСЕЛ
     * DataOutputStream - поток, создаваемый на основе файлового потока FileOutputStream
     * DataOutputStream позволяет легко записывать примитивные типы вроде int, double, byte и т.д. в файлы
     * 
     * @param numbers список чисел
     * @param fileName имя файла, в который будут записаны данные числа
     */
    public static void write(ArrayList<Integer> numbers, String fileName) {
        try(DataOutputStream out = new DataOutputStream(new FileOutputStream(fileName))) {
            for(Integer number : numbers) {
                out.writeInt(number);
            }
        } catch (IOException exception) {
            System.out.println("Что-то пошло не так");
        }
    }

    /**
     * Возвращает true, если число счастливое, т.е. сумма первых трёх его цифр равна сумме последних трёх
     * false в любом другом случае
     * 
     * @param number числоа для проверки на счастливость
     * @return true, если число счастливое, т.е. сумма первых трёх его цифр равна сумме последних трёх
     */
    public static boolean isLucky(Integer number) {
        StringBuilder number_builder = new StringBuilder();
        String number_string = number.toString();
        for(int i = 0; i < 6 - number_string.length(); i++) {
            number_builder.append(0);
        }
        number_builder.append(number_string);
        number_string = number_builder.toString();
        
        char a = 0, b = 0;
        for(int i = 0; i < 3; i++) {
            a += number_string.charAt(i);
            b += number_string.charAt(3 + i);
        }
        
        return a == b;
    }

}
