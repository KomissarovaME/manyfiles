package org.komissarova.many_files;

import java.util.ArrayList;

/**
 * ШАГ 3. Получение чисел с помощью чтения, удаление не счастливых и запись оставшихся в файл
 * 
 * @author Комиссарова М.Е., 16ИТ18к
 */
public class Stage3 {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = NumberIO.read("int6data.dat");
        System.out.println("Прочитаны числа: " + numbers);
        numbers = leaveOnlyLucky(numbers);
        System.out.println("Получены счастливые числа: " + numbers);
        NumberIO.writeAsText(numbers, "lucky.txt");
    }

    /**
     * Создаёт новый спсиок чисел, содержащий только счастливые числа
     * 
     * @param numbers список шестизначных чисел
     * @return список счастливых чисел из списка numbers
     */
    public static ArrayList<Integer> leaveOnlyLucky(ArrayList<Integer> numbers) {
        ArrayList<Integer> newNumbers = new ArrayList<Integer>();
        
        for(int number : numbers) {
            if(NumberIO.isLucky(number)) {
                newNumbers.add(number);
            }
        }
        
        return newNumbers;
    }
}
